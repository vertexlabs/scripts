#!/bin/bash

# From : https://www.postgresql.org/download/linux/ubuntu/

# Add the xenial-pgdg repo to the ap sources list
echo 'deb http://apt.postgresql.org/pub/repos/apt/ bionic-pgdg main' | sudo tee --append /etc/apt/sources.list.d/pgdg.list

# Grab the key for this repo
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -

# Update the package list
sudo apt-get update

# Install postgres 
sudo apt-get -y install postgresql-client-10 postgresql-10

