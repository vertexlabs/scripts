#!/bin/bash

# From https://zertrin.org/projects/iptables-persistent/

echo ------------------------------------------------------------------------------------------------
echo "Saving iptables configuration..."
echo ------------------------------------------------------------------------------------------------

sudo apt-get install -y iptables-persistent
sudo dpkg-reconfigure iptables-persistent
