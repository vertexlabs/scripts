#!/bin/bash
set -e

if [ "$#" -ne 2 ]; then
    echo "Usage : ./promote.sh <previous tier hostname> <application name>"
    exit -1
fi


PREVIOUS_ENVIRONMENT=$1 # this will be a hostname that has been configured for password-less ssh access
APPLICATION_NAME=$2 # this is the prefix to the wrapper script

DIV="-----------------------------------------------------------------"

echo ${DIV}
echo "Attempting to promote latest version of ${APPLICATION_NAME} from ${PREVIOUS_ENVIRONMENT}..."
echo ${DIV}

echo "Retrieving latest version from ${PREVIOUS_ENVIRONMENT}..."

LATEST_LINK=${APPLICATION_NAME}-latest

LATEST_VERSION=$(basename "$(ssh ${PREVIOUS_ENVIRONMENT} "readlink -f ${LATEST_LINK}")")

echo "Latest version is ${LATEST_VERSION}"

echo "Looking for file ${LATEST_LINK}..."

if [ -L ${LATEST_LINK} ]; then

    echo "Found local version"

    LOCAL_VERSION=$(basename "$(readlink -f ${LATEST_LINK})")

    echo "Local version is ${LOCAL_VERSION}"

    if [ -f ~/${LATEST_LINK}/bin/${APPLICATION_NAME}.sh ]; then
        echo "Stopping local version..."
        ~/${LATEST_LINK}/bin/${APPLICATION_NAME}.sh stop
    fi

    if [ -d ~/${LATEST_VERSION} ]; then
        echo "Deleting duplicate latest version folder that already exists locally..."
        rm -rf ~/${LATEST_VERSION}
    fi

    if [ -f ${LATEST_VERSION}.zip ]; then
       echo "Deleting zip for duplicate latest version"
    fi
else
    echo "No local version found, this must be the first version"
fi

echo "Copying ${LATEST_VERSION} to local machine from ${PREVIOUS_ENVIRONMENT}..."
scp ${PREVIOUS_ENVIRONMENT}:dist/${LATEST_VERSION}.zip .

echo "Decompressing..."
unzip ${LATEST_VERSION}

echo "Linking to ${LATEST_LINK}..."
rm -f latest
ln -sf ${LATEST_VERSION} ${LATEST_LINK}

echo "Starting ${LATEST_VERSION} locally, check output for start up errors..."
~/${LATEST_LINK}/bin/${APPLICATION_NAME}.sh start

echo "Done"
