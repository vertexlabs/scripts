#!/bin/bash

# create the newtech user
sudo useradd -d /home/newtech -m newtech -s /bin/bash

# copy over bash stuff
sudo install -C -m 775 -o newtech -g newtech ~/.bashrc /home/newtech

# switch to that user - suspect this won't work...
sudo su newtech <<EOF

# create .ssh and auth keys
cd ~
mkdir .ssh
cd .ssh
touch authorized_keys
nano authorized_keys

EOF
