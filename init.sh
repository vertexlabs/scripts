#!/bin/bash

# refresh the package list and upgrade everything currently installed
sudo apt-get update && sudo apt-get upgrade -y

# add some useful tools
sudo apt-get -y install ncdu htop unzip tree

# jdk
sudo apt-get -y install openjdk-8-jdk

echo --------------------------------------------------
echo                       Done
echo --------------------------------------------------



