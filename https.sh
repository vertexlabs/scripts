#!/bin/bash

TARGET_PORT=$1

echo ------------------------------------------------------------------------------------------------
echo "Configuring iptables to forward TCP traffic coming in on port 443 to target port ${TARGET_PORT}"
echo ------------------------------------------------------------------------------------------------

sudo iptables -t nat -A PREROUTING -p tcp --dport 443 -j REDIRECT --to-ports ${TARGET_PORT}
